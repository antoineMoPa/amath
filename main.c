#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "amath_strings.h"

#define AMATH_STRING_LENGTH 255
#define AMATH_MAX_CHILDREN 4


// TYPES DEFINITIONS
// Management / structure
#define TYPE_BLOCK                 0
// Operations
#define TYPE_OPERATION             1000
#define TYPE_OP_PLUS               1001
#define TYPE_OP_MINUS              1002
#define TYPE_OP_MULTIPLICATION     1003
#define TYPE_OP_DIVISION           1004
// Values
#define TYPE_VALUE                 2000
#define TYPE_VAL_INT               2001
// Variables
#define TYPE_VARIABLE              3000

/* not memory efficient but I don't give a shit */
typedef struct amath_atom_struct{
    int type;
    char str_input[AMATH_STRING_LENGTH];
    char str_val[AMATH_STRING_LENGTH];
    long int_val;
    float float_val;
    struct amath_atom_struct * children[AMATH_MAX_CHILDREN];
    int children_num;
} amath_atom;


void amath_read(char * str,int length){
    char buf;
    int i = 0;
    // promt input
    printf("> ");
    while(1){
        // read char
        buf = getchar();
        // line over?
        if(buf == '\n'){
            break;
        }
        // only append to string
        // if there is space left in string
        if(buf != '\n' && i < length - 1){
            str[i] = buf;
            i++;
        }
    }
    
    str[i] = '\0';
}

void amath_init_atom(amath_atom * atom){
    atom->children_num = 0;
    atom->str_input[0] = '\0';
    atom->str_val[0] = '\0';
}

/**
   Free atom memory recursively
   skips 'skip' levels from parent object
   re initiailize parent object if skip = 1
*/
void amath_free_atom(amath_atom * atom,int skip){
    int i;
    for(i = 0; i < atom->children_num;i++){
        amath_free_atom(atom->children[i],skip-1);
    }
    if(skip < 1){
        free(atom);
    } else {
        amath_init_atom(atom);
    }
}

amath_atom * amath_new_atom(){
    amath_atom * new_atom;
    new_atom = malloc(sizeof(amath_atom));
    amath_init_atom(new_atom);
    return new_atom;
}

/* Returns the ID of appended child */
int amath_add_atom(amath_atom * atom){
    atom->children[atom->children_num] = amath_new_atom();
    atom->children_num++;
    return atom->children_num - 1;
}

int amath_eval(char * str, amath_atom * result){
    int i = 0,j = 0;
    char * end;
    int child_num;
    int length = strlen(str);
    // counts number of parenthesis expected to close
    int pcount = 0;
    char substr[AMATH_STRING_LENGTH];
    strcpy(result->str_input,str);
    bool type_found = false;
    char * c_buffer;
    int operation_pos;
    bool first_block_found = false;
    bool last_block_found = false;
    // strings before and after an operation
    // @todo, more efficient string size
    char * str_before = malloc(sizeof(char) * (length-1));
    char * str_after = malloc(sizeof(char) * (length-1));

    printf("evaluating: %s\n",str);
    
    // decompose parentheses ()
    // and create atoms
    for(i = 0; i < length && !first_block_found;i++){
        if(str[i] == '('){
	    type_found = true;
	    result->type = TYPE_BLOCK;
            pcount = 1;
            i++;
            j = i;
            // find closing parenthesis
            while(pcount > 0 && j < length){
                j++;
                if(str[j] == '('){
                    pcount++;
                }
                if(str[j] == ')'){
                    pcount--;
                }
            }
            // Copy from i to j to substr
            strncpy(substr,str + i, j - i);
            substr[j - i] = '\0';
            child_num = amath_add_atom(result);
            amath_eval(
		       substr,
		       result->children[child_num]
		       );
	    
	    first_block_found = true;
	    i = j + 1;
            break;
        }
    }

    // if no more parentheses
    // Start managing operations

    // start at i if first block was found
    i = (first_block_found? i: 0);
    printf("rest: %s i: %i\n",str+i,i);
    if(!type_found || first_block_found){
	// look for +
	c_buffer = strchr(str + i,'+');
	if(c_buffer != 0){
	    type_found = true;
	    result->type = TYPE_OP_PLUS;
	    operation_pos = c_buffer - str;
	    if(!first_block_found){
		strncpy(str_before,str,operation_pos);
	    }
	    strncpy(str_after,
		    str + operation_pos + 1,
		    length-operation_pos);
	    
	    if(!first_block_found){
		// add part before
		child_num = amath_add_atom(result);
		amath_eval(
			   str_before,
			   result->children[child_num]
			   );
	    }
	    // add part after
	    child_num = amath_add_atom(result);
	    amath_eval(
		       str_after,
		       result->children[child_num]
		       );
	    printf("before: %s\n",str_before);
	    printf("after: %s\n",str_after);
	}
    }
    // manage numbers
    if(!type_found){
	printf("num match str: %s\n",str);
	// find out if str contains int
	if(amath_matches_only(str + i,"0123456789 ")){
	    printf("int found\n");
	    result->type = TYPE_VAL_INT;
	    result->int_val =
		strtol(
		       str + i,
		       &end,
		       10
		       );
	}
	
    }
    
    sprintf(result->str_val,"%i",42);
    return 1;
}

/**
   prints atom recursively
*/
void amath_printr(amath_atom * atom,int level){
    int i;
    // indentation
    for(i = 0; i < level; i++){
        printf(" ");
    }
    printf("%s\n",atom->str_input);
    for(i = 0; i < atom->children_num;i++){
        amath_printr(atom->children[i],level + 1);
    }
}


/**
   Read Eval Print Loop
*/
int amath_repl(){
    char input[AMATH_STRING_LENGTH];
    char str_result[AMATH_STRING_LENGTH];
    amath_atom * result;
    result = amath_new_atom();
    input[0] = '\0';
    
    // print(eval(read())) loop
    while(1){
        amath_free_atom(result,1);
        amath_read(input,AMATH_STRING_LENGTH);
        amath_eval(input,result);
        // exit
        if(strcmp(input,"exit") == 0){
            break;
        }
        amath_printr(result,0);
        printf("=> %s\n",result->str_val);
    }
    return 0;
}

int amath_eval_string(char * str){
    char str_result[AMATH_STRING_LENGTH];
    amath_atom * result;
    result = amath_new_atom();
    
    amath_eval(str,result);
    amath_printr(result,0);
    printf("=> %s\n",result->str_val);
    return 0;
}

int main(int argc, char ** argv){
    if(argc >= 3 && strcmp(argv[1],"-e") == 0){
        amath_eval_string(argv[2]);
    } else {
        amath_repl();
    }
}
