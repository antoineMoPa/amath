#include <string.h>
#include <stdbool.h>

/**
   looks wether chr belongs to the set chrs
 */
bool amath_chrchrs(char chr, char * chrs){
    int i;
    for(i = 0; i < strlen(chrs); i++){
	if(chr == chrs[i]){
	    return true;
	}
    }
    return false;
}


/**
   amath_does_str_contain_only_chrs
   that is a long function name
   so I changed it to
*/
bool amath_matches_only(char * str,char * chrs){
    int i;
    for(i = 0; i < strlen(str); i++){
	if(!amath_chrchrs(str[i],chrs)){
	    return false;
	}
    }
    return true;
}

int amath_number_end(char * str){
    int i=0,j=0;
    if(!amath_chrchrs(str[0],"0123456789")){
	return -1;
    }
    for(i = 0; i <= strlen(str); i++){
	if(amath_chrchrs(str[i],"0123456789")){
	    if(j == -1){
		j = i;
	    }
	} else {
	    if(j != -1){
		return j;
	    }
	    j = -1;
	}
    }
}

int amath_strtoint(char * str,int len){
    char * nstr = malloc(sizeof(char) * len);
    strncpy(nstr,str,len);
    int number;
    number = atoi(str);
    return number;
}
