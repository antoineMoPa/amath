#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../amath_strings.h"

void automata(char * str, int len){
    int i=0,j=0,end=0;
    // current form position
    int fp = 0;
    // form elements
    /*
      n: number
      o: operation
     */
    char f[] = "???";
    /*
      3: form elements
      2: 0 = begining in string
         1 = end
     */
    int els [3][2];
    
    // Remove spaces
    for(i = 0; i <= strlen(str); i++){
	if(str[i] != ' '){
	    str[j] = str[i];
	    j++;
	}
    }
    i=0;
    while(fp < 3 && i < strlen(str)){
	// do we have a number?
	end = amath_number_end(str+i);
	if(end != -1){
	    f[fp] = 'n';
	    els[fp][0] = i;
	    els[fp][1] = end;
	    fp++;
	    i = end + 1;
	}
	// do we have an operation?
	else if(amath_chrchrs(str[i],"+-*/")){
	    f[fp] = 'o';
	    els[fp][0] = i;
	    els[fp][1] = i;
	    fp++;
	}
	i++;
    }
    if(strcmp(f,"non") == 0){
	int n1 = amath_strtoint(str + els[0][0],els[fp][1]);
	char o = str[els[1][0]];
	int n2 = amath_strtoint(str + els[2][0],els[fp][1]);
	int result = 0;
	switch(o){
	case '+':
	    result = n1 + n2;
	    break;
	case '-':
	    result = n1 - n2;
	    break;
	case '*':
	    result = n1 * n2;
	    break;
	case '/':
	    result = n1 / n2;
	    break;
	}
    }
}

int main(int argc, char ** argv){
    char input[] = "13 + 39";
    int len = strlen(input);
    automata(input, len);
    printf("%s\n",input);
    return 0;
}
